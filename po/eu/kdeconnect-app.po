# Translation for kdeconnect-app.po to Euskara/Basque (eu).
# Copyright (C) 2020-2022, This file is copyright:
# This file is distributed under the same license as the kdeconnect-kde package.
# KDE euskaratzeko proiektuko arduraduna <xalba@ni.eus>.
#
# Translators:
# Iñigo Salvador Azurmendi <xalba@ni.eus>, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kdeconnect-kde\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-15 00:47+0000\n"
"PO-Revision-Date: 2022-12-17 09:39+0100\n"
"Last-Translator: Iñigo Salvador Azurmendi <xalba@ni.eus>\n"
"Language-Team: Basque\n"
"Language: eu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.12.0\n"

#: main.cpp:32 main.cpp:34
#, kde-format
msgid "KDE Connect"
msgstr "KDE Connect"

#: main.cpp:36
#, kde-format
msgid "(c) 2015, Aleix Pol Gonzalez"
msgstr "(c) 2015, Aleix Pol Gonzalez"

#: main.cpp:37
#, kde-format
msgid "Aleix Pol Gonzalez"
msgstr "Aleix Pol Gonzalez"

#: main.cpp:37
#, kde-format
msgid "Maintainer"
msgstr "Arduraduna"

#: main.cpp:38
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Iñigo Salvador Azurmendi"

#: main.cpp:38
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "xalba@ni.eus"

#: main.cpp:54
#, kde-format
msgid "URL to share"
msgstr "Partekatzeko URLa"

#: qml/DevicePage.qml:23
#, kde-format
msgid "Unpair"
msgstr "Desparekatu"

#: qml/DevicePage.qml:28
#, kde-format
msgid "Send Ping"
msgstr "Bidali Ping"

#: qml/DevicePage.qml:36 qml/PluginSettings.qml:15
#, kde-format
msgid "Plugin Settings"
msgstr "Pluginen ezarpenak"

#: qml/DevicePage.qml:60
#, kde-format
msgid "Multimedia control"
msgstr "Multimedia agintea"

#: qml/DevicePage.qml:67
#, kde-format
msgid "Remote input"
msgstr "Urrutiko sarrerakoa"

#: qml/DevicePage.qml:74 qml/presentationRemote.qml:15
#, kde-format
msgid "Presentation Remote"
msgstr "Aurkezpenetarako urruneko agintea"

#: qml/DevicePage.qml:83 qml/mousepad.qml:44
#, kde-format
msgid "Lock"
msgstr "Giltzatu"

#: qml/DevicePage.qml:83
#, kde-format
msgid "Unlock"
msgstr "Giltzapetik askatu"

#: qml/DevicePage.qml:90
#, kde-format
msgid "Find Device"
msgstr "Aurkitu nire gailua"

#: qml/DevicePage.qml:95 qml/runcommand.qml:16
#, kde-format
msgid "Run command"
msgstr "Exekutatu komandoa"

#: qml/DevicePage.qml:103
#, kde-format
msgid "Share File"
msgstr "Partekatu fitxategia"

#: qml/DevicePage.qml:108 qml/volume.qml:16
#, kde-format
msgid "Volume control"
msgstr "Bolumenaren agintea"

#: qml/DevicePage.qml:117
#, kde-format
msgid "This device is not paired"
msgstr "Gailu hau ez dago parekatuta"

#: qml/DevicePage.qml:121 qml/FindDevicesPage.qml:23
#, kde-format
msgid "Pair"
msgstr "Parekatu"

#: qml/DevicePage.qml:128
#, kde-format
msgid "Pair requested"
msgstr "Parekatzea eskatu da"

#: qml/DevicePage.qml:134
#, kde-format
msgid "Accept"
msgstr "Onartu"

#: qml/DevicePage.qml:140
#, kde-format
msgid "Reject"
msgstr "Errefuxatu"

#: qml/DevicePage.qml:149
#, kde-format
msgid "This device is not reachable"
msgstr "Gailu hau ez da irisgarria"

#: qml/DevicePage.qml:157
#, kde-format
msgid "Please choose a file"
msgstr "Mesedez aukeratu fitxategi bat"

#: qml/FindDevicesPage.qml:38
#, kde-format
msgid "No devices found"
msgstr "Ez da gailurik aurkitu"

#: qml/FindDevicesPage.qml:51
#, kde-format
msgid "Remembered"
msgstr "Oroitua"

#: qml/FindDevicesPage.qml:53
#, kde-format
msgid "Available"
msgstr "Erabilgarria"

#: qml/FindDevicesPage.qml:55
#, kde-format
msgid "Connected"
msgstr "Konektatua"

#: qml/main.qml:76
#, kde-format
msgid "Find devices..."
msgstr "Aurkitu gailuak..."

#: qml/main.qml:121 qml/main.qml:124
#, kde-format
msgid "Settings"
msgstr "Ezarpenak"

#: qml/mousepad.qml:16
#, kde-format
msgid "Remote Control"
msgstr "Urruneko agintea"

#: qml/mousepad.qml:59
#, kde-format
msgid "Press the left and right mouse buttons at the same time to unlock"
msgstr ""
"Sakatu saguaren ezker eta eskuin botoiak aldi berean giltzapetik askatzeko"

#: qml/mpris.qml:19
#, kde-format
msgid "Multimedia Controls"
msgstr "Multimedia aginteak"

#: qml/mpris.qml:61
#, kde-format
msgid "No players available"
msgstr "Ez dago jotzaile erabilgarririk"

#: qml/mpris.qml:109
#, kde-format
msgid "%1 - %2"
msgstr "%1 - %2"

#: qml/presentationRemote.qml:20
#, kde-format
msgid "Enable Full-Screen"
msgstr "Gaitu pantaila-betea"

#: qml/runcommand.qml:21
#, kde-format
msgid "Edit commands"
msgstr "Editatu komandoak"

#: qml/runcommand.qml:24
#, kde-format
msgid "You can edit commands on the connected device"
msgstr "Komandoak editatu ditzakezu konektatutako gailuan"

#: qml/runcommand.qml:43
#, kde-format
msgid "No commands defined"
msgstr "Ez da komandorik definitu"

#: qml/Settings.qml:13
#, kde-format
msgctxt "@title:window"
msgid "Settings"
msgstr "Ezarpenak"

#: qml/Settings.qml:35
#, kde-format
msgid "Device name"
msgstr "Gailuaren izena"

#: qml/Settings.qml:52
#, kde-format
msgid "About KDE Connect"
msgstr "KDE Connect-eri buruz"
