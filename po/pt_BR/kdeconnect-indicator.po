# Translation of kdeconnect-indicator.po to Brazilian Portuguese
# Copyright (C) 2020 This file is copyright:
# This file is distributed under the same license as the kdeconnect-kde package.
#
# André Marcelo Alvarenga <alvarenga@kde.org>, 2020.
# Luiz Fernando Ranghetti <elchevive@opensuse.org>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: kdeconnect-kde\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-12 00:45+0000\n"
"PO-Revision-Date: 2021-06-29 14:04-0300\n"
"Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>\n"
"Language-Team: Brazilian Portuguese <kde-i18n-pt_BR@kde.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 20.04.2\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "André Marcelo Alvarenga"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "alvarenga@kde.org"

#: deviceindicator.cpp:53
#, kde-format
msgid "Browse device"
msgstr "Navegar no dispositivo"

#: deviceindicator.cpp:67
#, kde-format
msgid "Ring device"
msgstr "Tocar dispositivo"

#: deviceindicator.cpp:81
#, kde-format
msgid "Get a photo"
msgstr "Tirar uma foto"

#: deviceindicator.cpp:101
#, kde-format
msgid "Send a file/URL"
msgstr "Enviar um arquivo/URL"

#: deviceindicator.cpp:111
#, kde-format
msgid "SMS Messages..."
msgstr "Mensagens SMS..."

#: deviceindicator.cpp:124
#, kde-format
msgid "Run command"
msgstr "Executar comando"

#: deviceindicator.cpp:126
#, kde-format
msgid "Add commands"
msgstr "Adicionar comandos"

#: indicatorhelper_mac.cpp:34
#, kde-format
msgid "Launching"
msgstr "Iniciando"

#: indicatorhelper_mac.cpp:84
#, kde-format
msgid "Launching daemon"
msgstr "Iniciando serviço"

#: indicatorhelper_mac.cpp:93
#, kde-format
msgid "Waiting D-Bus"
msgstr "Aguardando D-Bus"

#: indicatorhelper_mac.cpp:110 indicatorhelper_mac.cpp:120
#: indicatorhelper_mac.cpp:135
#, kde-format
msgid "KDE Connect"
msgstr "KDE Connect"

#: indicatorhelper_mac.cpp:111 indicatorhelper_mac.cpp:121
#, kde-format
msgid ""
"Cannot connect to DBus\n"
"KDE Connect will quit"
msgstr ""
"Não foi possível conectar ao D-Bus\n"
"O KDE Connect será fechado"

#: indicatorhelper_mac.cpp:135
#, kde-format
msgid "Cannot find kdeconnectd"
msgstr "Não foi possível encontrar o kdeconnectd"

#: indicatorhelper_mac.cpp:140
#, kde-format
msgid "Loading modules"
msgstr "Carregando módulos"

#: main.cpp:41
#, kde-format
msgid "KDE Connect Indicator"
msgstr "Indicador do KDE Connect"

#: main.cpp:43
#, kde-format
msgid "KDE Connect Indicator tool"
msgstr "Ferramenta Indicador do KDE Connect"

#: main.cpp:45
#, kde-format
msgid "(C) 2016 Aleix Pol Gonzalez"
msgstr "(C) 2016 Aleix Pol Gonzalez"

#: main.cpp:80
#, kde-format
msgid "Configure..."
msgstr "Configurar..."

#: main.cpp:102
#, kde-format
msgid "Pairing requests"
msgstr "Emparelhando pedidos"

#: main.cpp:107
#, kde-format
msgid "Pair"
msgstr "Emparelhar"

#: main.cpp:108
#, kde-format
msgid "Reject"
msgstr "Rejeitar"

#: main.cpp:114 main.cpp:124
#, kde-format
msgid "Quit"
msgstr "Sair"

#: main.cpp:143 main.cpp:167
#, kde-format
msgid "%1 device connected"
msgid_plural "%1 devices connected"
msgstr[0] "%1 dispositivo conectado"
msgstr[1] "%1 dispositivos conectados"

#: systray_actions/battery_action.cpp:28
#, kde-format
msgid "No Battery"
msgstr "Sem bateria"

#: systray_actions/battery_action.cpp:30
#, kde-format
msgid "Battery: %1% (Charging)"
msgstr "Bateria: %1% (Carregando)"

#: systray_actions/battery_action.cpp:32
#, kde-format
msgid "Battery: %1%"
msgstr "Bateria: %1%"

#: systray_actions/connectivity_action.cpp:30
#, kde-format
msgctxt ""
"The fallback text to display in case the remote device does not have a "
"cellular connection"
msgid "No Cellular Connectivity"
msgstr "Sem conectividade celular"

#: systray_actions/connectivity_action.cpp:33
#, kde-format
msgctxt ""
"Display the cellular connection type and an approximate percentage of signal "
"strength"
msgid "%1  | ~%2%"
msgstr "%1  | ~%2%"

#~ msgid "Select file to send to '%1'"
#~ msgstr "Selecione o arquivo a enviar para '%1'"
